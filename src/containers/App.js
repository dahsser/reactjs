import React, { useState } from 'react';
import classes from './App.module.css';
import Persons from '../components/Persons/Persons';
import Cockpit from '../components/Cockpit/Cockpit';
const App = (props) => {
	const [personsState, setPersons] = useState({
		persons: [
			{ id: 'abc1', name: 'Testing', age: 22 },
			{ id: 'abc2', name: 'Alejandro', age: 35 },
			{ id: 'abc3', name: 'Pedro', age: 26 },
			{ id: 'abc3', name: 'Assembler test', age: 26 },
		]
	});
	const [showResultsState, setShowResultsState] = useState(false);
	const deletePersonHandler = (personIndex) => {
		console.log("index:", personIndex)
		const persons = personsState.persons.slice();
		persons.splice(personIndex, 1);
		setPersons({ persons: persons })
	}

	const togglePersonHandler = () => {
		setShowResultsState(!showResultsState)
	}

	const nameChanceHandler = (event, id) => {
		const personIndex = personsState.persons.findIndex(p => {
			return p.id === id
		});
		const person = { ...personsState.persons[personIndex] }
		person.name = event.target.value;
		const persons = [...personsState.persons];
		persons[personIndex] = person;
		setPersons({
			persons: persons
		})
	}
	let persons = null;
	
	if (showResultsState) {
		persons = (
			<div>
				<Persons
					persons={personsState.persons}
					clicked={deletePersonHandler}
					changed={nameChanceHandler}
				/>
			</div>
		)

	}
	
	return (
		<div className={classes.App}>
			<Cockpit
				showPersons = {showResultsState}
				persons = {personsState.persons}
				clicked = {togglePersonHandler}
			/>
			{persons}
		</div>
	)
}

export default App;

