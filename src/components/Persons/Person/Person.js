import React from 'react';
import PersonStyle from './Person.module.css'
const person = (props) => {
	return (
		<div className={PersonStyle.Person}>
			<p onClick={props.click}>Testeando Name: {props.name} Age: {props.age}</p>
			<p>{props.children}</p>
			<input type="text" onChange={props.changed} value={props.name}/>
		</div>
	)
}

export default person;