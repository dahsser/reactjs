import React from 'react';
import classes from './Cockpit.module.css';
const cockpit = (props) => {
	const assignedClasses = [];
	let buttonClass = "";
	if(props.showPersons){
		buttonClass = classes.Red;
	}
	if (props.persons.length <= 2) {
		assignedClasses.push(classes.red);
	}
	if (props.persons.length <= 1) {
		assignedClasses.push(classes.bold);
	}
	return (
		<div className={classes.Cockpit}>
			<p className={assignedClasses.join(' ')}>Testing dahsser</p>
			<button className={buttonClass} onClick={() => props.clicked()}>Toggle persons</button>
		</div>
	);
}

export default cockpit;